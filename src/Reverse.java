import java.util.Arrays;

public class Reverse {
    String str;
    public Reverse(String str){
        this.str=new String(str);
    }

    public void reverseWord(){
        String arr[]=str.split("[.,/\s+/]");


        for (int iterate = 0; iterate < arr.length; iterate++) {

            StringBuilder stringBuilder=new StringBuilder(arr[iterate]);
            arr[iterate]=stringBuilder.reverse().toString();

        }

        System.out.println( Arrays.toString(arr).replace(",","").replace("[","").replace("]",""));

    }
}
